#include <util/sorted_cyclic_list.h>

#include <util/std_def.h>

struct scl *
new_scl( )
{
        return NULL;
};

struct scl *
free_scl( struct scl *scl )
{
        if ( scl == NULL)
                return NULL;
        struct scl *cur = scl->next;

        while ( cur != scl )
        {
                struct scl *tmp = cur->next;
                free( cur );
                cur = tmp;
        }

        free( scl );
        return NULL;
};

struct scl *
scl_insert( struct scl *scl, double weight, void *data )
{
        /*
         * SCL always points to the element with the smallest weight!
         */
        struct scl *new = malloc( sizeof( struct scl ) );
        new->weight = weight;
        new->data = data;
        if ( scl == NULL )
        {
                new->prev = new;
                new->next = new;
                return new;
        }
        else
        {
                if ( weight < scl->weight )
                {
                        new->prev = scl->prev;
                        scl->prev->next = new;
                        new->next = scl;
                        scl->prev = new;
                        return new;
                }
                else
                {
                        struct scl *cur = scl->next;
                        while ( cur != scl && cur->weight < weight )
                        {
                                cur = cur->next;
                        }
                        // insert new before cur
                        new->prev = cur->prev;
                        new->next = cur;
                        cur->prev->next = new;
                        cur->prev = new;
                        return scl;
                }
                
        }
}

struct scl *
scl_delete_( struct scl *scl )
{
        if ( scl->next == scl )
        {
                free( scl );
                return NULL;
        }
        else
        {
                scl->prev->next = scl->next;
                scl->next->prev = scl->prev;
                struct scl *ret = scl->next;
                free( scl );
                return ret;
        }
}

struct scl *
scl_find( struct scl *scl, void *data )
{
        if ( scl->data == data )
                return scl;
        struct scl *cur = scl->next;
        while ( cur != scl && cur->data != data )
                cur = cur->next;
        if ( cur->data == data )
                return cur;
        else
                return NULL;
}

struct scl *
scl_delete( struct scl *scl, void *data )
{
        if ( scl == NULL )
        {
                return NULL;
        }

        struct scl *cur = scl_find( scl, data );
        if ( cur == NULL )
        {
                return scl;
        }
        else if ( cur == scl )
        {
                return scl_delete_( scl );
        }
        else
        {
                scl_delete_( cur );
                return scl;
        }
        
}

void *
scl_get_prev( struct scl *scl, void *data )
{
        struct scl *f = scl_find( scl, data );
        if ( f == NULL || f == f->prev )
                return NULL;
        return f->prev->data;
}

void *
scl_pop_prev( struct scl **scl, void *data )
{
        struct scl *f = scl_find( *scl, data );
        if ( f == NULL || f == f->prev )
        {
                return NULL;
        }
        struct scl *to_pop = f->prev;
        void *ret = to_pop->data;
        if ( to_pop == *scl )
        {
                *scl = scl_delete_( to_pop );
        }
        else
        {
                scl_delete_( to_pop );
        }
        return ret;
}

void *
scl_get_next( struct scl *scl, void *data )
{
        struct scl *f = scl_find( scl, data );
        if ( f == NULL || f == f->next )
                return NULL;
        return f->next->data;
}

void *
scl_pop_next( struct scl **scl, void *data )
{
        struct scl *f = scl_find( *scl, data );
        if ( f == NULL || f == f->next )
        {
                return NULL;
        }
        struct scl *to_pop = f->next;
        void *ret = to_pop->data;
        if ( to_pop == *scl )
        {
                *scl = scl_delete_( to_pop );
        }
        else
        {
                scl_delete_( to_pop );
        }
        return ret;
}

void *
scl_get_min( struct scl *scl )
{
        return scl->data;
}

void *
scl_pop_min( struct scl **scl )
{
        void *ret = (*scl)->data;
        *scl=scl_delete_( *scl );
        return ret;
}

void *
scl_get_max( struct scl *scl )
{
        return scl->prev->data;
}

void *
scl_pop_max( struct scl **scl )
{
        void *ret = (*scl)->prev->data;
        if ( (*scl)->prev == (*scl) )
        {
                *scl=scl_delete_( (*scl) );
        }
        else
        {
                scl_delete_( (*scl)->prev );
        }
        return ret;
}
