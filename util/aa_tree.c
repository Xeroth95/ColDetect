#include "aa_tree.h"

#include <util/std_def.h>

#define EMPTY_TREE NULL

struct node
{
        size_t level;
        void *data;
        struct node *left;
        struct node *right;
};

struct node *
new_tree( )
{
        return EMPTY_TREE;
};

/*
 * returns a LEAF node. If the node is not used as LEAF,
 * then level has to be adjusted.
 */
struct node *
new_node( void *data )
{
        struct node *n = malloc( sizeof( struct node ) );
        n->data = data;
        n->left = EMPTY_TREE;
        n->right = EMPTY_TREE;
        n->level = 1;
}

struct node *
skew( struct node *t )
{
        if ( t == EMPTY_TREE )
        {
                return EMPTY_TREE;
        }
        else if ( t->left == EMPTY_TREE )
        {
                return t;
        }
        else if ( t->left->level == t->level )
        {
                /*
                 * Swap the pointers of the horizontal left links.
                 */
                struct node *tmp = t->left;
                t->left = tmp->right;
                tmp->right = t;
                return tmp;
        }
        else
        {
                return t;
        }
}

struct node *
split( struct node *t )
{
        if ( t == EMPTY_TREE )
        {
                return EMPTY_TREE;
        }
        else if ( t->right == EMPTY_TREE || t->right->right == EMPTY_TREE )
        {
                return t;
        }
        else if ( t->level == t->right->right->level )
        {
                /*
                 * We have two horizontal right links. Take the middle node,
                 * elevate it and return it.
                 */
                struct node *tmp = t->right;
                t->right = tmp->left;
                tmp->left = t;
                tmp->level = tmp->level + 1;
                return tmp;
        }
        else
        {
                return t;
        }
}

struct node *
insert( struct node *t, void *data, int (*compar)(void*, void*, void*),
        void* args)
{
        if ( t == EMPTY_TREE )
        {
                return new_node( data );
        }
        else if ( compar( data, t->data, args ) < 0 )
        {
                t->left = insert( t->left, data, compar, args );
        }
        else if ( compar( data, t->data, args ) >= /* ! */ 0 )
        {
                t->right = insert( t->right, data, compar, args );
        }

        /*
         * first a skew that a split.
         */
        
        t = skew( t );
        t = split( t );

        return t;
}

struct node *
succ( struct node *t )
{
        if ( t == EMPTY_TREE || t->right == EMPTY_TREE )
                return EMPTY_TREE;
        struct node *tmp = t->right;
        while ( tmp->left != NULL )
                tmp = tmp->left;
        return tmp;
}

struct node *
pred( struct node *t )
{
        if ( t == EMPTY_TREE || t->left == EMPTY_TREE )
                return EMPTY_TREE;
        struct node *tmp = t->left;
        while ( tmp->right != NULL )
                tmp = tmp->right;
        return tmp;
}

struct node *
decrease_level( struct node *t )
{
        size_t ll;
        size_t rl;
        size_t should_be;
        if ( t->left == EMPTY_TREE &&
             t->right == EMPTY_TREE )
        {
                ll = 0;
                rl = 0;
        }
        else
        {
                if ( t->left == EMPTY_TREE )
                        ll = 0;
                else
                        ll = t->left->level;

                if ( t->right == EMPTY_TREE )
                        rl = 0;
                else
                        rl = t->right->level;
        }
        should_be = (ll < rl) ? ll+1 : rl+1;
        if ( should_be < t->level )
        {
                t->level = should_be;
                if ( t->right != EMPTY_TREE && should_be < t->right->level )
                {
                        t->right->level = should_be;
                }
        }

        return t;
}

struct node *
delete( struct node *t, void *data, int (*compar)(void*, void*, void*),
        void* args )
{
        if ( t == EMPTY_TREE )
        {
                return EMPTY_TREE;
        }
        else if ( data == t->data )
        {
                // if t is a leaf
                if ( t->left == EMPTY_TREE &&
                     t->right == EMPTY_TREE )
                {
                        free( t );
                        return EMPTY_TREE;
                }
                else if ( t->left == EMPTY_TREE )
                {
                        struct node *suc = succ( t );
                        t->data = suc->data;
                        t->right = delete( t->right, suc->data, compar, args );
                }
                else
                {
                        struct node *prd = pred( t );
                        t->data = prd->data;
                        t->left = delete( t->left, prd->data, compar, args );
                }
        }
        else if ( compar( data, t->data, args ) < 0 )
        {
                t->left = delete( t->left, data, compar, args );
        }
        else /* if ( compar( data, t->data ) >= 0 ) */
        {
                t->right = delete( t->right, data, compar, args );
        }

        t = decrease_level( t );
        t = skew( t );

        t->right = skew( t->right );
        if ( t->right != EMPTY_TREE )
        {
                t->right->right = skew( t->right->right );
        }
        t = split( t );
        t->right = split( t->right );
        return t;
}

void **
get_pred( struct node *t, void *data, int (*compar)( void*, void*, void* ),
          void *args )
{
        if ( t == EMPTY_TREE )
        {
                return NULL;
        }
        
        if ( compar( data, t->data, args ) <= 0 )
        {
                return get_pred( t->left, data, compar, args );
        }
        else
        {
                void **l = get_pred( t->right, data, compar, args );
                if ( l == NULL )
                        return &t->data;
                else
                        return l;
        }
}

void *
get_succ( struct node *t, void *data, int (*compar)( void*, void*, void* ),
          void *args )
{
        if ( t == EMPTY_TREE )
        {
                return NULL;
        }
        else if ( data == t->data )
        {
                struct node *suc = succ( t );
                return suc->data;
        }
        else if ( compar( data, t->data, args ) < 0 )
        {
                get_pred( t->left, data, compar, args );
        }
        else
        {
                get_pred( t->right, data, compar, args );
        }
}

       

struct node *
delete_tree( struct node *t )
{
        if ( t == EMPTY_TREE )
                return EMPTY_TREE;

        t->left = delete_tree( t->left );
        t->right = delete_tree( t->right );
        free( t );
        return EMPTY_TREE;
}

size_t
num_left( struct node *t, void *data, int (*compar)( void*, void*, void* ),
          void *args )
{
        if ( t == EMPTY_TREE )
                return 0;
        else
        {
                int com = compar( data, t->data, args );
                if ( com < 0 )
                        return 1 + num_left( t->left, data, compar, args )
                                + num_left( t->right, data, compar, args );
                else
                        return num_left( t->left, data, compar, args );
        }
}

size_t
num_right( struct node *t, void *data, int (*compar)( void*, void*, void* ),
           void *args )
{
        if ( t == EMPTY_TREE )
                return 0;
        else
        {
                int com = compar( data, t->data, args );
                if ( com > 0 )
                        return 1 + num_right( t->left, data, compar, args )
                                + num_right( t->right, data, compar, args );
                else
                        return num_right( t->right, data, compar, args );
        }
}


#undef EMPTY_TREE
