#if !defined( SORTED_CYCLIC_LIST_H__ )
#define SORTED_CYCLIC_LIST_H__

struct scl
{
        double weight;
        void *data;
        struct scl *prev;
        struct scl *next;
};

struct scl *
new_scl( );

struct scl *
free_scl( struct scl *scl );

struct scl *
scl_insert( struct scl *scl, double weight, void *data );

struct scl *
scl_delete( struct scl *scl, void *data );

void *
scl_get_prev( struct scl *scl, void *data );

void *
scl_pop_prev( struct scl **scl, void *data );

void *
scl_get_next( struct scl *scl, void *data );

void *
scl_pop_next( struct scl **scl, void *data );

void *
scl_get_min( struct scl *scl );

void *
scl_pop_min( struct scl **scl );

void *
scl_get_max( struct scl *scl );

void *
scl_pop_max( struct scl **scl );

#endif /* SORTED_CYCLIC_LIST_H__ */
