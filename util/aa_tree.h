#if !defined( AA_TREE_H__ )
#define AA_TREE_H__

#include <util/std_def.h>

typedef int (*compare_fun)( void*, void*, void* );

struct node;

struct node *
new_tree( );

struct node *
insert( struct node *t, void *data, compare_fun compar, void *args );

struct node *
delete( struct node *t, void *data, compare_fun compar, void *args );

void **
get_pred( struct node *t, void *data, compare_fun compar, void *args );

void *
get_succ( struct node *t, void *data, compare_fun compar, void *args );

struct node *
delete_tree( struct node *t );

size_t
num_left( struct node *t, void *data, compare_fun compar, void *args );

size_t
num_right( struct node *t, void *data, compare_fun compar, void *args );

#endif /* AA_TREE_H__ */
