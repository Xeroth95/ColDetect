#if !defined(POINT_H__)
#define POINT_H__

struct point
{
        double x, y;
};

double point_len( struct point p );
double point_dist( struct point p_1, struct point p_2 );

int compar_point_p( struct point *p, struct point *q );
int compar_point( struct point p, struct point q );

#endif /* POINT_H__ */
