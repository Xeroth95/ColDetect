#if !defined( POLYGON_LIST_H__ )
#define POLYGON_LIST_H__

#include <util/std_def.h>

struct cyc_list
{
        index i;
        struct cyc_list *prev;
        struct cyc_list *next;
};

struct pol_list
{
        index num_points;
        index num_pol;
        index *index_to_pol;
        index *pol_size;
        struct cyc_list **pol_list;
};

#endif /* POLYGON_LIST_H__ */
