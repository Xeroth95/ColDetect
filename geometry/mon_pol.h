#if !defined(MON_POL_H__)
#define MON_POL_H__

#include <stdlib.h>

#include <util/std_def.h>

#include <geometry/point.h>

index *
mon_pol_triangulate( index num_points,
                     struct point monotone_pol[num_points] );

void
mon_pol_triangulate_into( struct point points[],
                          index monotone_polygon[],
                          index num_points, // in the monotone polygon
                          index triangules[],
                          index start );
#endif /* MON_POL_H__ */
