#include <geometry/inter_pol.h>

#include <assert.h>
#include <stdio.h>

#include <util/std_def.h>

#include <geometry/line.h>


struct scl *
add_diag_to_scl( struct point *points,
                 index num_points,
                 index start,
                 index end,
                 struct scl *scl )
{
        index bef_start = (start+num_points-1) % num_points;
        struct point bef_st = points[bef_start];
        struct point start_p = points[start];
        struct point end_p = points[end];
        double angle = angle_left( bef_st, start_p, end_p );
        return scl_insert( scl,
                           angle,
                           INDEX_TO_P( end ));
}

struct inter_pol *
make_inter_pol( index num_points, struct point points[num_points] )
{
        struct inter_pol *pol = malloc( sizeof( struct inter_pol ) );
        pol->num_points = num_points;
        pol->points = points;
        pol->num_diags = calloc( num_points, sizeof( index ) );
        pol->scls = calloc( num_points, sizeof( struct scl * ) );
        for ( index i = 0; i < num_points; ++i )
        {
                
                
                pol->scls[i] = new_scl( );
                /*
                 * Since these diagonals at the border of the polygon
                 * only belong to 1 polygon at the end. They are only
                 * added in one direction.
                 */
                index left = (i+num_points-1) % num_points;
                index right = (i+1) % num_points;
                pol->scls[i] = add_diag_to_scl( points, num_points,
                                                i, right,
                                                pol->scls[i] );
                pol->scls[i] = add_diag_to_scl( points, num_points,
                                                i, left,
                                                pol->scls[i] );
                pol->num_diags[i] = 1;
        }
        pol->num_additional_diagonals = 0;
        return pol;
};

struct inter_pol *
free_inter_pol( struct inter_pol *pol )
{
        for ( index i = 0; i < pol->num_points; ++i )
        {
                pol->scls[i] = free_scl( pol->scls[i] );
        }
        free( pol->num_diags );
        free( pol );
        return NULL;
}

void
add_diagonal( struct inter_pol *pol, index i, index j )
{
        assert( i < pol->num_points );
        assert( j < pol->num_points );

        /*
         * Since every interior diagonal belongs to
         * 2 different polygons at the end
         * we add it in 2 different directions
         */
        
        pol->scls[i] = add_diag_to_scl( pol->points,
                                        pol->num_points,
                                        i, j,
                                        pol->scls[i] );
        pol->scls[j] = add_diag_to_scl( pol->points,
                                        pol->num_points,
                                        j, i,
                                        pol->scls[j] );
        pol->num_additional_diagonals += 1;
        pol->num_diags[i] += 1;
        pol->num_diags[j] += 1;
}


void
remove_diagonal( struct inter_pol *pol, index i, index j )
{
        assert( i < pol->num_points );
        assert( j < pol->num_points );
        assert( pol->num_additional_diagonals > 0 );
        assert( pol->num_diags[i] > 0 );
        assert( pol->num_diags[j] > 0 );
        
        pol->scls[i] = scl_delete( pol->scls[i],
                                   INDEX_TO_P( j ) );
        pol->scls[j] = scl_delete( pol->scls[j],
                                   INDEX_TO_P( i ) );
        pol->num_additional_diagonals -= 1;
        pol->num_diags[i] -= 1;
        pol->num_diags[j] -= 1;
}

index
get_created_polygon_into( struct inter_pol *pol, index *array )
{
        index cv = 0;
        while ( pol->num_diags[cv] == 0 )
                        ++cv;
        assert( cv < pol->num_points );
        // print the first polygon starting at cv

        index counter = 0;
        
        index prev = cv;
        index cur = P_TO_INDEX( scl_pop_max( &pol->scls[cv] ) );
        if ( (cur+1) % pol->num_points == prev )
        {
                // dont go backwards on the polygon border!
                cur = P_TO_INDEX( scl_pop_max( &pol->scls[cv] ) );
        }
        pol->num_diags[cv] -= 1;
        array[counter++] = prev;
        while ( cur != cv )
        {
                array[counter++] = cur;
                pol->num_diags[cur] -= 1;
                index next = P_TO_INDEX( scl_pop_prev( &pol->scls[cur],
                                                       INDEX_TO_P(prev) ) );
                
                // dont go backwards on the border
                // of the polygon!
                if ( (next+1) % pol->num_points == cur )
                        next = P_TO_INDEX( scl_pop_prev( &pol->scls[cur],
                                                         INDEX_TO_P(prev) ) );
                prev = cur;
                cur = next;
        }

        return counter;
}

index *
get_created_polygon( struct inter_pol *pol, index *size )
{
        index *ind = malloc( pol->num_points * sizeof( index ) );
        *size = get_created_polygon_into( pol, ind );
        return ind;
}

index **
get_created_polygons( struct inter_pol *pol )
{
        index num_points = pol->num_points;
        index num_pols = pol->num_additional_diagonals+1;
        index cv = 0; // current vertex
        for ( index i = 0; i < num_pols; ++i )
        {
                printf( "START POLYGON %u/%u\n", i+1, num_pols );
                while ( pol->num_diags[cv] == 0 )
                        ++cv;
                assert( cv < num_points );
                // print the first polygon starting at cv
                
                index prev = cv;
                index cur = P_TO_INDEX( scl_pop_max( &pol->scls[cv] ) );
                if ( (cur+1) % pol->num_points == prev )
                {
                        // dont go backwards on the polygon border!
                        cur = P_TO_INDEX( scl_pop_max( &pol->scls[cv] ) );
                }
                pol->num_diags[cv] -= 1;
                printf( "\tvertex: %u\n", prev );
                printf( "\tvertex: %u\n", cur );
                while ( cur != cv )
                {
                        pol->num_diags[cur] -= 1;
                        index next = P_TO_INDEX( scl_pop_prev( &pol->scls[cur],
                                                               INDEX_TO_P(prev) ) );

                        // dont go backwards on the border
                        // of the polygon!
                        if ( (next+1) % num_points == cur )
                                next = P_TO_INDEX( scl_pop_prev( &pol->scls[cur],
                                                                 INDEX_TO_P(prev) ) );
                        printf( "\tvertex: %u\n", next );
                        prev = cur;
                        cur = next;
                }
                printf( "END POLYGON\n\n" );

                                                   
        }

        return NULL;
        
};
