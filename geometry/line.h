#if !defined( LINE_H__ )
#define LINE_H__

#include "point.h"

enum lpos { LEFT_OF_LINE, RIGHT_OF_LINE, ON_LINE };


/*
 * Tests wether the point x is on the line/left of the line/right of the line
 * spanned by s and e
 */
enum lpos
position( struct point s, struct point e, struct point x );

/*
 * Returns the angle from the left side of s->e between s->e and e->x
 */
double
angle_left( struct point s, struct point e, struct point x );
#endif /* LINE_H__ */
