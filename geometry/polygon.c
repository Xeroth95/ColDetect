#include <geometry/polygon.h>

#include <util/std_def.h>

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>

#include <geometry/mon_pol.h>
#include <geometry/inter_pol.h>
#include <util/aa_tree.h>
#include <geometry/line.h>

/* in string.h */
extern void *memcpy( void*, const void *, size_t );

enum vertex_type { MERGE, SPLIT, START, END, REG_L, REG_R, NUM };
char *vertex_type_name[NUM] = { "MERGE", "SPLIT", "START", "END",
                                 "REG_L", "REG_R" };

struct box *
bound( size_t num_points, struct point points[num_points] )
{
        assert( num_points > 0 );
        double min_x = points[0].x, min_y = points[0].y;
        double max_x = points[0].x, max_y = points[0].y;
        for ( size_t i = 1; i < num_points; ++i )
        {
                double cur_x = points[i].x;
                double cur_y = points[i].y;

                if ( cur_x > max_x )
                        max_x = cur_x;
                else if ( cur_x < min_x )
                        min_x = cur_x;
                
                if ( cur_y > max_y )
                        max_y = cur_y;
                else if ( cur_y < min_y )
                        min_y = cur_y;
                
        }

        struct box *bounding_box = calloc( 1, sizeof( struct box ) );
        bounding_box->bot_left.x = min_x;
        bounding_box->bot_left.y = min_y;
        bounding_box->width = max_x - min_x;
        bounding_box->height = max_y - min_y;

        return bounding_box;
}

// TODO: use the normal dist function
static double
_dist( struct point p1, struct point p2 )
{
#define SQ(x) ((x) * (x))
        return sqrt( SQ(p2.x - p1.x) + SQ(p2.y-p1.y) );
#undef SQ
                     
}

// the "easy" O(n^2) algorithm :(
index *
triangulate( index num_points, struct point points[num_points] )
{
        if ( num_points < 2 )
                return NULL;
        //return triangulize_monotone_polygon( num_points, points );

        struct inter_pol *ipol = make_inter_pol( num_points, points );

        // we use a sweep line to construct the monotone polygons
        // first we need to sort points
        index sorted[num_points];
        for ( index i = 0; i < num_points; ++i )
        {
                sorted[i] = i;
        }
        
        int _comp( const void *pi, const void *pj )
        {
                index i = *(index *) pi;
                index j = *(index *) pj;
                struct point p1 = points[i];
                struct point p2 = points[j];
                if ( p1.y == p2.y )
                        return p1.x < p2.x ? -1 : 1;
                else
                        return p2.y < p1.y ? -1 : 1;
        };
        
        qsort( sorted, num_points, sizeof( index ),
               _comp );

        index current_index = 0;
        struct node *tree = new_tree( );
        index helper[num_points];

        enum vertex_type types[num_points];
        for ( index mid = 0; mid < num_points; ++mid )
        {
                index left = (mid+num_points-1) % num_points;
                index right = (mid+1) % num_points;

                if ( points[left].y == points[mid].y &&
                     points[right].y == points[mid].y )
                {
                        types[mid] = START;
                }
                else if ( points[left].y >= points[mid].y &&
                          points[mid].y >= points[right].y )
                {
                        types[mid] = REG_L;
                }
                else if ( points[right].y > points[mid].y &&
                          points[mid].y >= points[left].y )
                {
                        types[mid] = REG_R;
                }
                else if ( points[mid].y >= points[left].y &&
                          points[mid].y >= points[right].y )
                {
                        if ( points[left].x < points[right].x )
                        {
                                types[mid] = SPLIT;
                        }
                        else if ( points[left].x == points[right].x )
                        {
                                if ( points[left].y < points[right].y )
                                {
                                        types[mid] = SPLIT;
                                }
                                else if ( points[left].y == points[right].y )
                                        assert( 0 );
                                else
                                {
                                        types[mid] = START;
                                }
                        }
                        else
                        {
                                types[mid] = START;
                        }
                }
                else if ( points[left].y >= points[mid].y &&
                          points[right].y >= points[mid].y )
                {
                        if ( points[left].x < points[right].x )
                        {
                                types[mid] = END;
                        }
                        else if ( points[left].x == points[right].x )
                        {
                                if ( points[left].y > points[right].y )
                                {
                                        types[mid] = END;
                                }
                                else if ( points[left].y == points[right].y )
                                        assert( 0 );
                                else
                                {
                                        types[mid] = MERGE;
                                }
                        }
                        else
                        {
                                types[mid] = MERGE;
                        }
                }
                printf("types[%u] = %s\n", mid, vertex_type_name[types[mid]] );
        }

        // edge i = edge formed by vertex i -> vertex i+1
        
        // edge_edge_compar
        int e_e_c( void *pi, void *pj, void *py )
        {
                index i = P_TO_INDEX(pi);
                index i1 = (i+1) % num_points;
                index j = P_TO_INDEX(pj);
                index j1 = (j+1) % num_points;
                double y = *(double *) py;

                double x11 = points[i].x;
                double y11 = points[i].y;
                double x12 = points[i1].x;
                double y12 = points[i1].y;
                double x21 = points[j].x;
                double y21 = points[j].y;
                double x22 = points[j1].x;
                double y22 = points[j1].y;

                //printf( "Testing edge %u with edge %u at y = %lf\n",
                //        i, j, y );

                // edge 1 = p11 -> p12
                // edge 2 = p21 -> p22

                double x1_at_y; // y-intersect with e1
                double x2_at_y; // y-intersect with e2

                // compute x1_at_y

                if ( y11 == y12 )
                {
                        x1_at_y = x11;
                }
                else
                {
                        x1_at_y = (( y - y11 ) / (y12 - y11)) * (x12-x11) + x11;
                }

                if ( y21 == y22 )
                {
                        x2_at_y = x21;
                }
                else
                {
                        x2_at_y = ((y - y21) / (y22 - y21)) * (x22 - x21) + x21;
                }
                
                return x1_at_y < x2_at_y ? -1 : 1;
        }

        // vertex_edge_compar
        int e_v_c ( void *pi, void *pl, void *py )
        {
                index left = P_TO_INDEX(pl);
                index right = (left+1) % num_points;

                double x1 = points[left].x;
                double y1 = points[left].y;
                double x2 = points[right].x;
                double y2 = points[right].y;
                
                index vertex =  P_TO_INDEX(pi);
                double y = *(double *) py;

                //printf( "Testing edge %u with vertex %u at y = %lf\n",
                //        left, vertex, y );

                double x_at_y;

                if ( y1 == y2 )
                {
                        x_at_y = x1;
                }
                else
                {
                        x_at_y = ((y-y1) / (y2 - y1)) * (x2 - x1) + x1;
                }

                return points[vertex].x < x_at_y ? -1 : 1;
        }
        
        while ( current_index < num_points )
        {
                // mid = current vertex
                index mid = sorted[current_index];
                index left = (mid+num_points-1) % num_points;
                index right = (mid+1) % num_points;
                printf( "Looking at (%u, %u, %u)\n",
                        left, mid, right );
                switch ( types[mid] )
                {
                case START: {
                        tree = insert( tree, INDEX_TO_P(mid), e_e_c,
                                       (void *) &points[mid].y );
                        helper[mid] = mid;
                        break;
                }
                case END: {
                        if ( types[helper[left]] == MERGE )
                        {
                                // ADD DIAGONAL helper[left] -> mid
                                printf("DIAG %u -> %u\n", helper[left], mid );
                                add_diagonal( ipol, mid, helper[left] );
                        }
                        tree = delete( tree, INDEX_TO_P(left), e_e_c,
                                       /* or maybe points[left] */
                                       (void *) &points[mid].y );
                        break;
                }
                case SPLIT: {
                        void **pj = get_pred( tree, INDEX_TO_P(mid), e_v_c,
                                              (void *) &points[mid].y );
                        if ( pj == NULL )
                                assert( 0 );
                        index j = P_TO_INDEX(*pj);
                        // ADD DIAGONAL mid -> helper[j]
                        printf("DIAG %u -> %u\n", mid, helper[j] );
                        add_diagonal( ipol, mid, helper[j] );
                        helper[j] = mid;
                        tree = insert( tree, INDEX_TO_P(mid), e_e_c,
                                       (void *) &points[mid].y );
                        helper[mid] = mid;
                        break;
                }
                case MERGE: {
                        if ( types[helper[left]] == MERGE )
                        {
                                // ADD DIAGONAL helper[left] -> mid
                                printf("DIAG %u -> %u\n", helper[left], mid );
                                add_diagonal( ipol, mid, helper[left] );
                        }
                        tree = delete( tree, INDEX_TO_P(left), e_e_c,
                                       (void *) &points[mid].y );
                        void **pj = get_pred( tree, INDEX_TO_P(mid), e_v_c,
                                              (void *) &points[mid].y );
                        if ( pj == NULL )
                                assert( 0 );
                        index j = P_TO_INDEX(*pj);
                        
                        if ( types[helper[j]] == MERGE )
                        {
                                // ADD DIAGONAL mid -> helper[j]
                                printf("DIAG %u -> %u\n", mid, helper[j] );
                                add_diagonal( ipol, mid, helper[j] );
                        }
                        helper[j] = mid;
                        break;
                }
                case REG_L: {
                        if ( types[helper[left]] == MERGE )
                        {
                               // ADD DIAGONAL helper[left] -> mid
                               printf("DIAG %u -> %u\n", helper[left], mid );
                               add_diagonal( ipol, mid, helper[left] );
                        }
                        tree = delete( tree, INDEX_TO_P(left), e_e_c,
                                       (void *) &points[mid].y );
                        tree = insert( tree, INDEX_TO_P(mid), e_e_c,
                                       (void *) &points[mid].y );
                        helper[mid] = mid;
                        break;
                }
                case REG_R: {
                        void **pj = get_pred( tree, INDEX_TO_P(mid), e_v_c,
                                              (void *) &points[mid].y );
                        if ( pj == NULL )
                                assert( 0 );
                        index j = P_TO_INDEX(*pj);
                        if ( types[helper[j]] == MERGE )
                        {
                                // ADD DIAGONAL mid -> helper[j]
                                printf("DIAG %u -> %u\n", mid, helper[j] );
                                add_diagonal( ipol, mid, helper[j] );
                        }
                        helper[j] = mid;
                        break;
                }
                }
                ++current_index;
        }

        index pol_size;
        index *triangles = malloc( sizeof( index ) * (num_points - 2) * 3 );
        index num_pols = ipol->num_additional_diagonals+1;
        index *pol;
        index num_triangles = 0;
        for ( index i = 0; i < num_pols; ++i )
        {
                if ( i == 0)
                {
                        pol = get_created_polygon( ipol, &pol_size );
                }
                else
                {
                        pol_size = get_created_polygon_into( ipol, pol );
                }
                printf( "START POLYGON %u/%u (SIZE %u)\n", i+1, num_pols,
                        pol_size );
                for ( index j = 0; j < pol_size; ++j )
                {
                        
                        printf( "EDGE %u\n", pol[j] );
                }
                printf( "END POLYGON\n" );
                
                mon_pol_triangulate_into( points,
                                          pol,
                                          pol_size,
                                          triangles,
                                          num_triangles );
                
                num_triangles += pol_size-2;
        }
        free( pol );
        free_inter_pol( ipol );
        
        delete_tree( tree );
        return triangles;
}

void
reverse( index num_points, struct point points[num_points] )
{
        for ( index i = 0; i < (num_points / 2); ++i )
        {
                struct point p = points[i];
                points[i] = points[num_points-(i+1)];
                points[num_points-(i+1)] = p;
        }
}

void
make_counter_clockwise( index num_points, struct point points[num_points] )
{
        // check wether the points are in clockwise order
        // see: http://stackoverflow.com/a/1165943

        double val = 0;
        index i = 0;
        for ( i = 0; i+1 < num_points; ++i )
        {
                val += (points[i+1].x - points[i].x)*
                        (points[i+1].y + points[i].y);
                
        }
        val += (points[0].x - points[i].x) * (points[0].y + points[i].y);

        if ( val > 0 )
        {
                // the points are in clockwise order
                reverse( num_points, points );
        }
        // now the points are guarantied to be in counter-clockwise order
}

struct polygon *
make_polygon_var( index num_points, ... )
{
        va_list argp;
        va_start( argp, num_points );
        struct point *points = malloc( sizeof( struct point ) * num_points );
        for ( index i = 0; i < num_points; ++i )
        {
                points[i] = (struct point) va_arg( argp, struct point );
        }
        struct polygon *p = make_polygon( num_points, points );
        // make_polygon makes a copy of the points!
        free( points );
        return p;
}

struct polygon *
make_polygon( index num_points, struct point ipoints[num_points] )
{
        struct point *points = malloc( sizeof( struct point ) * num_points );
        memcpy( points, ipoints, num_points * sizeof( struct point ) );
        make_counter_clockwise( num_points, points );
        struct polygon *pol = calloc( 1, sizeof( struct polygon ) );
        pol->num_points = num_points;
        pol->points = points;
        pol->triangles = triangulate( num_points, points );
        pol->bounding_box = bound( num_points, points );
}

