#include <geometry/line.h>

#include <math.h>

#if !defined(M_PI)
#define M_PI (acos(-1.0))
#endif

#include <geometry/point.h>

enum lpos
position( struct point s, struct point e, struct point p )
{
        double d = (e.x - s.x)*(p.y - e.y) - (e.y - s.y)*(p.x - s.x);
        if ( d > 0 )
                return LEFT_OF_LINE;
        else if ( d < 0 )
                return RIGHT_OF_LINE;
        else
                return ON_LINE;
}

double
angle_left( struct point s, struct point e, struct point p )
{
        enum lpos pos = position( s, e, p );

        struct point v = { .x = e.x - s.x,
                           .y = e.y - s.y };

        struct point w = { .x = p.x - e.x,
                           .y = p.y - e.y };
        
        double len_v = point_len( v );
        double len_w = point_len( w );
        
        double angle = acos( (v.x * w.x + v.y * w.y) / ( len_v * len_w ) );

        switch ( pos )
        {
        case ON_LINE:
                return 0.0d;
        case LEFT_OF_LINE:
                return angle;
        case RIGHT_OF_LINE:
                return 2*M_PI - angle;
        }
}

#if defined(LINE_DEBUG)

void
main( void )
{
        struct point p1 = { 0.0, 0.0 };
        struct point p2 = { 1.0, 0.0 };
        struct point p3 = { 0.5, 0.5 };

        enum lpos test = position( &p1, &p2, &p3 );

        if ( test == LEFT_OF_LINE )
        {
                printf("LEFT\n");
        } else {
                printf("RIGHT\n");
        }
        
}

#endif
