#if !defined(TRIANGLE_H__)
#define TRIANGLE_H__


#include "point.h"

struct triangle
{
        /*
         * The three defining points of the triangle
         */
        struct point *a, *b, *c;
        /*
         * Side lengths of the triangle
         */
        double side_a, side_b, side_c;
};

#endif /* TRIANGLE_H__ */
