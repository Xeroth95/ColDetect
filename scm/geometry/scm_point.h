/*
 * This function initalises the point type
 * for use in the GNU Guile SCM shell
 */
void
init_point_type( void );

/*
 * Makes a point smob from two double smobs.
 */
SCM
make_point( SCM s_x, SCM s_y );

/*
 * Returns the point inside a smob.
 */
struct point *
get_point( SCM s_p );

/*
 * Prints a representation of the point into the port.
 */
int
print_point( SCM s_point, SCM port, scm_print_state* pstate );

/*
 * Returns SCM_BOOL_T iff the contained points define the same
 * point in the plane.
 */
SCM
equal_point( SCM s_p1, SCM s_p2 );

SCM
s_point_x( SCM s_p );

SCM
s_point_y( SCM s_p );

SCM
s_point_len( SCM s_p );

SCM
s_point_dist( SCM s_p1, SCM s_p2 );
