#include "shader.h"

#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>

static GLchar const *
read_shader( char const *shadername )
{
        FILE *f = fopen( shadername, "rb" );
        if ( f == NULL )
        {
                fprintf( stderr,
                         "Could not open shader \"%s\"\n",
                         shadername );
        }

        fseek( f, 0, SEEK_END );
        int len = ftell( f );
        rewind( f );
        GLchar *source = calloc( 1, sizeof( GLchar ) * (len+1) );

        fread( source, 1, len, f );
        fclose( f );

        source[len] = '\0';

        return (GLchar const *) source;
}

GLuint
load_shaders( struct shader_info pipeline[] )
{
        GLuint program = glCreateProgram( );
        int cur = 0;
        struct shader_info *entry = &pipeline[cur];
        while ( entry->type != GL_NONE )
        {
                GLuint shader = glCreateShader( entry->type );

                entry->shader = shader;
                GLchar const *source = read_shader( entry->name );
                if ( source == NULL )
                {
                        for ( int i = 0; i <= cur; ++i )
                        {
                                glDetachShader( program, pipeline[i].shader );
                                glDeleteShader( pipeline[i].shader );
                                pipeline[i].shader = 0;
                        }
                        glDeleteProgram( program );
                        return 0;
                }

                glShaderSource( shader, 1, &source, NULL );
                free( (void *) source );
                glCompileShader( shader );

                GLint compiled;
                glGetShaderiv( shader, GL_COMPILE_STATUS, &compiled );
                if ( compiled == 0 )
                {
                        GLsizei len;
                        glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &len );
                        GLchar *log = calloc( 1, sizeof( GLchar ) * (len+1) );

                        glGetShaderInfoLog( shader, len, &len, log );
                        fprintf( stderr,
                                 "Could not compile shader \"%s\":\n"
                                 "-------------------------------\n"
                                 "%s\n\n",
                                 entry->name,
                                 log );
                        free( log );
                        for ( int i = 0; i <= cur; ++i )
                        {
                                glDetachShader( program, pipeline[i].shader );
                                glDeleteShader( pipeline[i].shader );
                                pipeline[i].shader = 0;
                        }
                        glDeleteProgram( program );

                        return 0;
                }

                glAttachShader( program, shader );
                cur = cur + 1;
                entry = &pipeline[cur];

        }
        glLinkProgram( program );
        GLint linked;
        glGetProgramiv( program, GL_LINK_STATUS, &linked );
        if ( linked == 0 )
        {
                GLsizei len;
                glGetProgramiv( program, GL_INFO_LOG_LENGTH, &len );

                GLchar *log = calloc(1, sizeof( GLchar ) * (len+1));
                glGetProgramInfoLog( program, len, &len, log );
                fprintf( stderr,
                         "Could not link program:\n"
                         "-------------------------------\n"
                         "%s\n\n",
                         log );
                free( log );
                for ( int i = 0; i < cur; ++i )
                {
                        glDetachShader( program, pipeline[i].shader );
                        glDeleteShader( pipeline[i].shader );
                        pipeline[i].shader = 0;
                }
                glDeleteProgram( program );
                
                return 0;
        }
        return program;
}
