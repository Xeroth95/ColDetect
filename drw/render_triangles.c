#include <stdio.h>
#include <stdlib.h>

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <assert.h>

#include "shader.h"
#include <geometry/point.h>
#include <geometry/polygon.h>

#include <drw/drawable_polygon.h>

static size_t HEIGHT = 800;
static size_t WIDTH = 640;

size_t num_dpol = 1;
struct dpol *dpols[1];

void
init( void )
{
        struct shader_info shaders[] = {
                { GL_VERTEX_SHADER, "vert.shader" },
                { GL_FRAGMENT_SHADER, "frag.shader" },
                { GL_NONE, NULL }
        };
        
        GLuint program = load_shaders( shaders );
        glUseProgram( program );
        glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
}

void
draw( struct dpol *dpol )
{

        if ( dpol->internal->num_points < 3 )
                return;
        GLsizei num_elem = 3*(dpol->internal->num_points-2);
        printf( "%u\n", num_elem );
        glBindVertexArray( dpol->vao );
        glBindBuffer( GL_ARRAY_BUFFER, dpol->buffer );
        glDrawElements( GL_TRIANGLES, num_elem,
                        GL_UNSIGNED_INT, dpol->internal->triangles );

}

void
display( void )
{
        glClear( GL_COLOR_BUFFER_BIT );
        for ( size_t i = 0; i < num_dpol; ++i )
        {
                draw( dpols[i] );
        }
        
        glFlush();

        GLenum glErr = glGetError();
        
        if (glErr != GL_NO_ERROR)  {
                fprintf(stderr, "Error: %s\n",
                        gluErrorString( glErr ));
                exit( EXIT_FAILURE );
        }

}

int
main( int argc, char *argv[] )
{
        glutInit( &argc, argv );
        glutInitDisplayMode( GLUT_RGBA );
        glutInitWindowSize( WIDTH, HEIGHT );
        glutInitContextVersion( 3, 1 );
        glutInitContextProfile( GLUT_CORE_PROFILE );
        glutCreateWindow(argv[0]);
        
        GLenum glErr = glGetError();
        
        if (glErr != GL_NO_ERROR)  {
                fprintf(stderr, "GlutInitError: %s\n",
                        gluErrorString( glErr ));
                exit( EXIT_FAILURE );
        }
        
        glewExperimental = GL_TRUE;
        GLenum err = glewInit();
        if ( GLEW_OK != err ) {
                fprintf(stderr, "GlewInitError: %s\n",
                        glewGetErrorString( err ));
                exit( EXIT_FAILURE );
        }
        
        init();

#define NORM(x) ((x) / 4.0)
        
        struct point points[] = { { NORM(-2), NORM(4) },
                                  { NORM(-2.5), NORM(2.5) },
                                  { NORM(-2), NORM(0.5) },
                                  { NORM(-1.5), NORM(1) },
                                  { NORM(-1), NORM(0.5) },
                                  { NORM(-0.5), NORM(1) },
                                  { NORM(1), NORM(1) },
                                  { NORM(2), NORM(0.5) },
                                  { NORM(3), NORM(1.5) },
                                  { NORM(1.5), NORM(3) },
                                  { NORM(1), NORM(3) },
                                  { NORM(-0.5), NORM(4) },
                                  { NORM(-1), NORM(3) } };
#undef NORM
        size_t num_points = sizeof( points ) / sizeof( struct point );

        struct polygon *p = make_polygon( num_points, points );

        dpols[0] = make_dpol( p );
        glutDisplayFunc( display );
        glutMainLoop();
}
